Name:           perl-HTML-Parser
Version:        3.83
Release:        2
Summary:        HTML parser
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/HTML-Parser
Source0:        https://cpan.metacpan.org/authors/id/O/OA/OALDERS/HTML-Parser-%{version}.tar.gz

BuildRequires:  coreutils findutils glibc-common gcc
BuildRequires:  perl-devel perl-generators perl-interpreter perl(Carp)
BuildRequires:  perl(Config) perl(Exporter) perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(File::Spec) perl(FileHandle) perl(HTML::Tagset) >= 3
BuildRequires:  perl(HTTP::Headers) perl(IO::File) perl(SelectSaver)
BuildRequires:  perl(strict) perl(Test) perl(Test::More) perl(threads)
BuildRequires:  perl(URI) perl(vars) perl(XSLoader)
Requires:       perl(HTML::Tagset) >= 3 perl(HTTP::Headers) perl(IO::File) perl(URI)

%{?perl_default_filter}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}perl\\(HTML::Tagset\\)$

%description
Objects of the HTML::Parser class will recognize markup and separate it
from plain text (alias data content) in HTML documents. As different kinds
of markup and text are recognized, the corresponding event handlers are invoked.

%package_help

%prep
%autosetup -n  HTML-Parser-%{version} -p1
chmod -c a-x eg/*

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}


%check
make test

%files
%{perl_vendorarch}/HTML/
%{perl_vendorarch}/auto/HTML/

%files help
%doc Changes README TODO eg/
%{_mandir}/man3/*.3pm*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 3.83-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Sep 30 2024 xiejing <xiejing@kylinos.cn> - 3.83-1
- Upgrade to version 3.83
- fix '$\/]' in HTML::Entities::encode_entities

* Fri Mar 15 2024 wangshuo <wangshuo@kylinos.cn> - 3.82-1
- Upgrade to version 3.82

* Wed Jun 14 2023 chenchen <chen_aka_jan@163.com> - 3.81-1
- Upgrade to version 3.81

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 3.78-1
- Upgrade to version 3.78

* Fri Dec 3  2021 guozhaorui <guozhaorui1@huawei.com> -3.76-1
- update to 3.76-1

* Wed Jun 23 2021 shangyibin <shangyibin1@openeuler.org> - 3.75-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add gcc to BuildRequires

* Thu Oct 29 2020 SimpleUpdate Robot <tc@openeuler.org> - 3.75-1
- Upgrade to version 3.75

* Mon Oct 21 2019 Zaiwang Li <lizaiwang1@huawei.com> - 3.72-16
- Init Package.

